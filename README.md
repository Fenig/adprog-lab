Name: GREGORIUS APRISUNNEA

NPM: 1706039710

Hobby: Praying

Class: B



Link DDP 2: https://gitlab.com/Fenig/Assignments/tree/master/assignment-4
Notes: 

1. Find a reason why you should use git branches: 

Git branches need to be done in order to maintain the development proccess so that many people can work on different branches at the same time and then can be merged together.

2. Describe how you use git branches on that repository: 

a. create the branch by using: git checkout -b <name>. 
b. change the things wanted to be changed. 
c. add, commit, push. 
d. You can go to master and try to merge. (or submit a merge request). 

3. Find a scenario so you should use git revert: 

Git revert can be done whn you want to return to previous version of your project. For example is when you make a mistake then you want to restart it from the yesterday's code. You can use git revert for that.

4. Describe how you use git revert on that repository: 

a. git log. 
b. find the hash on the location you want to revert back to. 
c. do: git checkout <hash> to check and explore the old project. 
d. do: git revert <hash> and change what you want to change on the desired branch. 
e. do: git add *, then do: git revert --continue. 